<?php
/**
 * PriceList.php
 *
 * @author: chazer
 * @created: 07.03.15 15:22
 */

namespace BillManager\YiiModels\Models;

use BillManager\YiiModels\Traits\Named;

class PriceList extends CommonEntity
{
    use Named;

    public $id;
    public $name;
    public $type;
    public $parent;

    /**
     * @param string $class
     * @return PriceList
     */
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{pricelist}}';
    }

    /**
     * @return null|BillItemType
     */
    public function getItemType()
    {
        return BillItemType::model()->findByPk($this->type);
    }

    public function groups()
    {
        $criteria = new \CDbCriteria();
        $criteria->addColumnCondition([
            'pgroup' => null,
            'parent' => null,
        ]);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    /**
     * @param int|PriceListGroup $group
     * @return $this
     */
    public function withGroup($group)
    {
        if ($group instanceof PriceListGroup)
            $group = $group->id;

        $criteria = new \CDbCriteria();
        $criteria->addColumnCondition([
            'pgroup' => $group,
        ]);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    /**
     * @param int|ServiceKind $servicekind
     * @return $this
     */
    public function withServiceKind($servicekind)
    {
        if ($servicekind instanceof ServiceKind)
            $servicekind = $servicekind->id;

        $criteria = new \CDbCriteria();
        $criteria->addColumnCondition([
            'servicekind' => $servicekind,
        ]);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public function isGroup()
    {
        return $this->pgroup === null and $this->parent === null;
    }

    public function isParent()
    {
        return $this->getAttribute('parent') === null && $this->getAttribute('addontype') === null;
    }

    /**
     * @return PriceListItem[]
     */
    public function getItems()
    {
        return PriceListItem::model()->findAllByParent($this);
    }

    /**
     * @return PriceListGroup|null
     */
    public function getPriceGroup()
    {
        if ($this->parent) {
            /** @var PriceList $parent */
            if ($parent = PriceList::model()->findByPk($this->parent)) {
                return $parent->getPriceGroup();
            }
        }
        /** @var PriceList $price */
        if ($this->pgroup) {
            return PriceListGroup::model()->findByPk($this->pgroup);
        }
        return null;
    }

    /**
     * @return PriceListItem
     */
    public function toAddon()
    {
        return ($this instanceof PriceListItem)
            ? $this
            : PriceListItem::model()->findByPk($this->primaryKey);
    }

    /**
     * @param int|BillItemType $type
     * @return $this
     */
    public function withItemType($type)
    {
        $type = ($type instanceof BillItemType) ? $type->id : (int) $type;
        $this->getDbCriteria()->addColumnCondition(['type' => $type]);
        return $this;
    }
}
