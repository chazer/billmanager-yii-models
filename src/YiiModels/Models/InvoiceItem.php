<?php
/**
 * InvoiceItem.php
 *
 * @author: chazer
 * @created: 25.10.15 2:52
 */

namespace BillManager\YiiModels\Models;

use CDbCriteria;

class InvoiceItem extends CommonEntity
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var int */
    public $invoice;

    /** @var int */
    public $item;

    /** @var float */
    public $amount;

    /** @var float */
    public $taxamount;

    /** @var float */
    public $taxrate;

    /**
     * @param string $class
     * @return InvoiceItem
     */
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{invoiceitem}}';
    }

    /**
     * @param int|Invoice $invoice
     * @return $this
     */
    public function withInvoice($invoice)
    {
        $invoice = ($invoice instanceof Invoice) ? $invoice->id : intval($invoice);
        $this->getDbCriteria()
            ->addColumnCondition(['invoice' => $invoice]);
        return $this;
    }

    /**
     * @return null|Item
     */
    public function getItem()
    {
        return Item::model()
            ->findByPk($this->item);
    }

    /**
     * @return Expense[]
     */
    public function getExpenses()
    {
        /** @var InvoiceItemToExpense[] $links */
        $links = InvoiceItemToExpense::model()
            ->withInvoiceItem($this)
            ->findAll();

        /** @var int[] $expenses */
        $expenses = [];
        if ($links) {
            foreach ($links as $link) {
                if ($expense = $link->expense) {
                    $expenses[$expense] = $expense;
                }
            }
        }

        /** @var ExpenseToCredit[] $links */
        $links = ExpenseToCredit::model()
            ->withInvoice($this->invoice)
            ->findAll();

        if ($links) {
            foreach ($links as $link) {
                if ($expense = $link->expense) {
                    $expenses[$expense] = $expense;
                }
            }
        }

        $itemId = (int) $this->item;

        if (!$itemId) {
            return [];
        }

        $cr = new CDbCriteria();
        $cr->addInCondition('id', $expenses);

        return Expense::model()
            ->withItem($itemId)
            ->findAll($cr);
    }
}
