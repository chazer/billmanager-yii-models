<?php
/**
 * CommonEntity.php
 *
 * @author: chazer
 * @created: 13.02.15 13:11
 */

namespace BillManager\YiiModels\Models;

use CActiveRecord;
use CDbConnection;
use Yii;

class CommonEntity extends CActiveRecord
{
    public static $db;

    /**
     * @return CDbConnection
     */
    public function getDbConnection()
    {
        if(self::$db!==null)
            return self::$db;
        else
        {
            return self::$db = Yii::app()->getComponent('billingdb');
        }
    }
}
