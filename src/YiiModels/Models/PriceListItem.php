<?php
/**
 * PriceListItem.php
 *
 * @author: chazer
 * @created: 24.08.15 18:40
 */

namespace BillManager\YiiModels\Models;

class PriceListItem extends PriceList
{
    /** @var int */
    public $rlimit;

    /** @var int */
    public $addonmeasure;

    /** @var int */
    public $addontype;

    /**
     * @param string $class
     * @return PriceListItem
     */
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function defaultScope()
    {
        return [
            'condition' => 't.parent IS NOT NULL',
        ];
    }

    /**
     * @param PriceList $pl
     * @return PriceListItem[]
     */
    public function findAllByParent(PriceList $pl)
    {
        return $this->findAllByAttributes(['parent' => $pl->id]);
    }

    /**
     * @return PriceList
     */
    public function getParent()
    {
        return PriceList::model()->findByPk($this->parent);
    }

    /**
     * @return Measure|null
     */
    public function getMeasure()
    {
        $measureId = $this->getAttribute('addonmeasure');
        return $measureId ? Measure::model()->findByPk($measureId) : null;
    }

    /**
     * @param int|PriceList $price
     * @return $this
     */
    public function withParent($price)
    {
        $pid = ($price instanceof PriceList) ? $price->id : (int) $price;
        $this->getDbCriteria()->addColumnCondition(['parent' => $pid]);
        return $this;
    }
}
