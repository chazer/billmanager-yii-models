<?php
/**
 * BillItemType.php
 *
 * @author: chazer
 * @created: 13.02.15 15:18
 */

namespace BillManager\YiiModels\Models;

use BillManager\YiiModels\Traits\Named;

class BillItemType extends CommonEntity
{
    use Named;

    /** @var int */
    public $id;

    /** @var string */
    public $intname;

    /**
     * @param string $class
     * @return BillItemType
     */
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{itemtype}}';
    }
}
