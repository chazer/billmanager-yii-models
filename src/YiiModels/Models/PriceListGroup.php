<?php
/**
 * PriceListGroup.php
 *
 * @author: chazer
 * @created: 24.08.15 16:22
 */

namespace BillManager\YiiModels\Models;

class PriceListGroup extends PriceList
{
    /**
     * @param string $class
     * @return PriceListGroup
     */
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function defaultScope()
    {
        return [
            'condition' => 't.pgroup IS NULL AND t.parent IS NULL',
        ];
    }

    /**
     * @return PriceList[]
     */
    public function getPriceLists()
    {
        /** @var PriceList $prices */
        return PriceList::model()->withGroup($this)->findAll();
    }
}
