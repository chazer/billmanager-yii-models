<?php
/**
 * Item.php
 *
 * @author: chazer
 * @created: 11.11.15 13:12
 */

namespace BillManager\YiiModels\Models;

class Item extends CommonEntity
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var int */
    public $account;

    /** @var int */
    public $price;

    /** @var int */
    public $period;

    /** @var int */
    public $parent;

    /** @var int */
    public $server;

    /** @var int */
    public $datacenter;

    /** @var int */
    public $enumitem;

    /** @var int */
    public $status;

    const STATUS_ORDERED = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_STOPPED = 3;
    const STATUS_DELETED = 4;
    const STATUS_PROCESSED = 5;

    /** @var float */
    public $amount;

    /** @var float */
    public $plimit;

    /** @var string */
    public $createdate;

    /** @var string */
    public $updatedate;

    /** @var string */
    public $suspenddate;

    /** @var string */
    public $deletedate;

    /** @var string */
    public $billdate;

    /** @var string */
    public $expiredate;

    /** @var string */
    public $opendate;

    /** @var string */
    public $paydate;

    /**
     * @param string $class
     * @return Item
     */
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{item}}';
    }

    /**
     * @return PriceList
     */
    public function getPriceList()
    {
        return PriceList::model()->findByPk($this->price);
    }

    /**
     * @return PriceList
     */
    public function whenIsParent()
    {
        $this->getDbCriteria()
            ->addInCondition('parent', [null]);
        return $this;
    }

    /**
     * @return PriceList
     */
    public function whenIsChild()
    {
        $this->getDbCriteria()
            ->addNotInCondition('parent', [null]);
        return $this;
    }

    /**
     * @param int|Item $item
     * @return $this
     */
    public function whenIsChildOf($item)
    {
        $parent = ($item instanceof Item) ? $item->id : (int) $item;
        $this->getDbCriteria()
            ->addColumnCondition(['parent' => $parent]);
        return $this;
    }

    /**
     * @return bool
     */
    public function isParent()
    {
        return empty($this->parent);
    }

    /**
     * @return bool
     */
    public function isChild()
    {
        return !$this->isParent();
    }

    /**
     * @return $this|null
     */
    public function getParent()
    {
        return $this->parent
            ? self::model()->findByPk($this->parent)
            : null;
    }

    /**
     * @return BillItemType|null
     */
    public function getType()
    {
        return ($price = $this->getPriceList())
            ? $price->getItemType()
            : null;
    }
}
