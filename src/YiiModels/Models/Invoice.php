<?php
/**
 * Invoice.php
 *
 * @author: chazer
 * @created: 29.10.15 18:31
 */

namespace BillManager\YiiModels\Models;

class Invoice extends CommonEntity
{
    /** @var int */
    public $id;

    /** @var int */
    public $provider;

    /** @var int */
    public $customer;

    /** @var string */
    public $num;

    /** @var string */
    public $cdate;

    /** @var int */
    public $currency;

    /** @var float */
    public $amount;

    /** @var float */
    public $num1c;

    /** @var int */
    public $contract;

    /** @var int */
    public $status;

    /** @var string */
    public $sdate;

    /**
     * @param string $class
     * @return Invoice
     */
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{invoice}}';
    }

    public function getDate1c()
    {
        return $this->getAttribute('1cdate');
    }

    public function setDate1c($value)
    {
        $this->setAttribute('1cdate', $value);
    }

    /**
     * @return InvoiceItem[]
     */
    public function getInvoiceItems()
    {
        return InvoiceItem::model()
            ->withInvoice($this)
            ->findAll();
    }

    /**
     * @return Currency
     */
    public function getCurrency()
    {
        return Currency::model()
            ->findByPk($this->currency);
    }
}
