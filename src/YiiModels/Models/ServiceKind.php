<?php
/**
 * ServiceKind.php
 *
 * @author: chazer
 * @created: 24.08.15 15:40
 */

namespace BillManager\YiiModels\Models;

/**
 * Class ServiceKind
 *
 * @property int $id
 * @property string $name
 * @property string $name_ru
 *
 * @package BillManager\Models
 */
class ServiceKind extends CommonEntity
{
    /**
     * @param string $class
     * @return ServiceKind
     */
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{servicekind}}';
    }

    public function getName($lang = null)
    {
        $prop = 'name';
        $alt = $prop . '_' . $lang;
        ($lang && $this->hasAttribute($alt)) || $alt = $prop;
        return $this->$alt ?: $this->$prop;
    }
}
