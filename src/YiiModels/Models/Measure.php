<?php
/**
 * Measure.php
 *
 * @author: chazer
 * @created: 13.11.15 17:36
 */

namespace BillManager\YiiModels\Models;

use BillManager\YiiModels\Traits\Named;

class Measure extends CommonEntity
{
    use Named;

    /** @var int */
    public $id;

    /** @var int */
    public $lessmeasure;

    private static $typesTable = [];
    /** @var float[] */
    private static $relationTable = [];

    /**
     * @param string $class
     * @return Measure
     */
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{measure}}';
    }

    /**
     * @return Measure|null
     */
    protected function getNextLowMeasure()
    {
        if (!isset($this->lessmeasure))
            return null;
        return Measure::model()->findByPk($this->lessmeasure);
    }

    /**
     * @return int
     */
    public function getType()
    {
        if (isset(self::$typesTable[$this->id]))
            return self::$typesTable[$this->id];
        $low = $this->getNextLowMeasure();
        $type = $low ? $low->getType() : $this->id;
        return self::$typesTable[$this->id] = $type;
    }

    /**
     * @param bool $precise
     * @return float|int
     */
    public function getFullRelation($precise = false)
    {
        if (!isset(self::$relationTable[$this->id])) {
            $relation = doubleval($this->relation);
            if ($low = $this->getNextLowMeasure()) {
                $relation *= $low->getFullRelation();
            }
            self::$relationTable[$this->id] = $relation;
        }
        $value = self::$relationTable[$this->id];
        return $precise ? $value : intval($value);
    }

    /**
     * @param mixed $value
     * @param int|Measure $measure
     * @param bool $precise
     * @return float|int
     */
    public function convertTo($value, $measure, $precise = false)
    {
        $value = doubleval($value);
        if (is_object($measure) && $measure instanceof Measure) {
            $m = $measure;
        } else {
            $m = Measure::model()->findByPk($measure);
        }
        $a = $this->getFullRelation($precise);
        $b = $m->getFullRelation($precise);
        $value = $value * $a / $b;
        return $precise ? $value : intval($value);
    }

    /**
     * @return array|Measure[]
     */
    protected function getHighMeasures()
    {
        /** @var Measure[] $hi */
        $hi = Measure::model()->findAllByAttributes(['lessmeasure' => $this->id]);
        $out = $hi;
        foreach ($hi as $model) {
            $measures = $model->getHighMeasures();
            foreach($measures as $m) {
                $out[] = $m;
            }
        }
        return $out;
    }

    /**
     * @param bool $min return only minimal element
     * @return Measure[]
     */
    protected function getLowMeasures($min = false)
    {
        $out = [];
        if ($low = $this->getNextLowMeasure()) {
            $measures = $low->getLowMeasures($min);
            if (count($measures) > 0) {
                foreach($measures as $m)
                    $out[] = $m;
            }
            if (!$min || !count($out))
                $out[] = $low;
        }
        return $out;
    }
}
