<?php
/**
 * Expense.php
 *
 * @author: chazer
 * @created: 11.11.15 12:53
 */

namespace BillManager\YiiModels\Models;

class Expense extends CommonEntity
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var int */
    public $subaccount;

    /** @var int */
    public $item;

    /** @var string */
    public $operation;

    /** @var string */
    public $params;

    /** @var string */
    public $cdate;

    /** @var float */
    public $amount;

    /** @var float */
    public $quantity;

    /** @var float */
    public $notpayd;

    /** @var int */
    public $orderitem;

    /**
     * @param string $class
     * @return Expense
     */
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{expense}}';
    }

    /**
     * @return null|Item
     */
    public function getItem()
    {
        return Item::model()
            ->findByPk($this->item);
    }

    /**
     * @param int|Item $item
     * @return $this
     */
    public function withItem($item)
    {
        $item = ($item instanceof Item) ? $item->id : intval($item);
        $this->getDbCriteria()
            ->addColumnCondition(['item' => $item]);
        return $this;
    }
}
