<?php
/**
 * InvoiceItemToExpense.php
 *
 * @author: chazer
 * @created: 11.11.15 12:52
 */

namespace BillManager\YiiModels\Models;

class InvoiceItemToExpense extends CommonEntity
{
    /** @var int */
    public $invoiceitem;

    /** @var int */
    public $expense;

    /** @var float */
    public $amount;

    /** @var float */
    public $nativeamount;

    /**
     * @param string $class
     * @return InvoiceItemToExpense
     */
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{invoiceitem2expense}}';
    }

    /**
     * @return Expense
     */
    public function getExpense()
    {
        return Expense::model()
            ->findByPk($this->expense);
    }

    /**
     * @param int|InvoiceItem $invoiceItem
     * @return $this
     */
    public function withInvoiceItem($invoiceItem)
    {
        $id = ($invoiceItem instanceof InvoiceItem) ? $invoiceItem->id : intval($invoiceItem);
        $this->getDbCriteria()
            ->addColumnCondition(['invoiceitem' => $id]);
        return $this;
    }
}
