<?php
/**
 * Currency.php
 *
 * @author: chazer
 * @created: 29.10.15 18:41
 */

namespace BillManager\YiiModels\Models;

class Currency extends CommonEntity
{
    /** @var string */
    public $name;

    /** @var string */
    public $iso;

    /** @var int */
    public $code;

    /**
     * @param string $class
     * @return Invoice
     */
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{currency}}';
    }
}
