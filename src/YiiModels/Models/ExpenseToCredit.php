<?php
/**
 * ExpenseToCredit.php
 *
 * @author: chazer
 * @created: 16.11.15 21:00
 */

namespace BillManager\YiiModels\Models;

/**
 * Class ExpenseToCredit
 *
 * @package BillManager\YiiModels\Models
 */
class ExpenseToCredit extends CommonEntity
{
    /** @var int */
    public $expense;

    /** @var int */
    public $credit;

    /** @var float */
    public $amount;

    /** @var float */
    public $taxamount;

    /** @var int */
    public $invoice;

    /**
     * @param string $class
     * @return ExpenseToCredit
     */
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{expense2credit}}';
    }

    /**
     * @param int|Invoice $invoice
     * @return $this
     */
    public function withInvoice($invoice)
    {
        $invoice = ($invoice instanceof Invoice) ? $invoice->id : intval($invoice);
        $this->getDbCriteria()
            ->addColumnCondition(['invoice' => $invoice]);
        return $this;
    }

    /**
     * @param int|Expense $expense
     * @return $this
     */
    public function withExpense($expense)
    {
        $expense = ($expense instanceof Expense) ? $expense->id : intval($expense);
        $this->getDbCriteria()
            ->addColumnCondition(['expense' => $expense]);
        return $this;
    }

    /**
     * @return null|Expense
     */
    public function getExpense()
    {
        return Expense::model()
            ->findByPk($this->expense);
    }

    /**
     * @return null|Invoice
     */
    public function getInvoice()
    {
        return Invoice::model()
            ->findByPk($this->invoice);
    }
}
