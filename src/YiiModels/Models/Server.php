<?php
/**
 * Server.php
 *
 * @author: chazer
 * @created: 23.11.15 16:53
 */

namespace BillManager\YiiModels\Models;

class Server extends CommonEntity
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var int */
    public $datacenter;

    /** @var string */
    public $ip;

    /** @var string */
    public $cpmodule;

    /** @var string */
    public $url;

    /** @var string */
    public $username;

    /** @var string */
    public $password;

    /** @var int */
    public $rlimit;

    /** @var string */
    public $statdate;

    /**
     * @param string $class
     * @return Server
     */
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{server}}';
    }
}
