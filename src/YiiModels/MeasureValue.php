<?php
/**
 * MeasureValue.php
 *
 * @author: chazer
 * @created: 23.02.15 18:18
 */

namespace BillManager\YiiModels;

use BillManager\YiiModels\Models\Measure;

/**
 * Class MeasureValue
 *
 * @package BillManager
 */
class MeasureValue
{
    public $value;

    public $measure;

    /**
     * @param int $value
     * @param int $measure
     */
    public function __construct($value, $measure)
    {
        $this->value = (int) $value;
        $this->measure = (int) $measure;
    }

    /**
     * @param $value
     * @param $measure
     */
    public function addValue($value, $measure)
    {
        $this->value += MeasureValue::convert($value, $measure, $this->measure);
    }

    /**
     * @param MeasureValue $value
     */
    public function add(MeasureValue $value)
    {
        $this->addValue($value->value, $value->measure);
    }

    /**
     * @param MeasureValue $value
     * @return bool
     */
    public function equal(MeasureValue $value)
    {
        return $this->value == $value->value && $this->measure == $value->measure;
    }

    public static function convert($value, $from, $to)
    {
        /** @var Measure $measure */
        $measure = Measure::model()->findByPk($from);
        return $measure->convertTo($value, $to);
    }
}
