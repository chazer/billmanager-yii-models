<?php
/**
 * Named.php
 *
 * @author: chazer
 * @created: 13.11.15 18:51
 */

namespace BillManager\YiiModels\Traits;

use CActiveRecord;

/**
 * Class Named
 *
 * @method bool hasAttribute()
 *
 * @package BillManager
 */
trait Named
{
    public $name;

    public function getName($lang = null)
    {
        $prop = 'name';
        $alt = $prop . '_' . $lang;
        ($lang && $this->hasAttribute($alt)) || $alt = $prop;
        return $this->$alt ?: $this->$prop;
    }
}
